package reportCard;

import java.util.Scanner;

/**
 * Student Reports & Details and calculations are went from here.
 * 
 * @author J.Konsikan
 * @version 1.0
 *
 */
public class StudentDetails {
	/**
	 * @param grade form this parameter all of sections get which grade is?..
	 */
	public void teacherPanel(int grade) {
		StudentDetails calling = new StudentDetails();
		PrintingMessages message = new PrintingMessages();
		message.studentDetailsMain();
		// YOU CAN SEE|ADD THE STUDENT DETAILS HERE
		// ADD NEW STUDENT DETAILS | 2-VIEW TERM 1 STUDENT DETAILS
		int correction = 1;
		while (correction == 1) {
			Scanner input = new Scanner(System.in);
			message.type();
			int type = input.nextInt();
			System.out.println();
			if (type == 1) {
				calling.addOrManageNewStudentDetails(grade);
				break;
			} else if (type == 2) {
				if (grade == 12) {
					calling.grade12Term1StudentDetails(1);
				}
				if (grade == 13) {
					calling.grade13Term1StudentDetails(1);
				}
				break;
			} else {
				correction = 1;
				message.incorrectValue();
			}
		}
	}

	/**
	 * @param grade form this parameter all of sections get which grade is?..
	 */
	public void addOrManageNewStudentDetails(int grade) {
		PrintingMessages message = new PrintingMessages();
		StudentDetails select = new StudentDetails();
		Scanner input = new Scanner(System.in);
		int correction = 1;
		while (correction == 1) {
			message.studentDetailsAddOrManageNewStudentDetails(1, "I");
			// TERM!
			int term = input.nextInt();
			System.out.println();
			if (term == 1) {
				message.studentDetailsAddOrManageNewStudentDetails(2, "I");
				// DETAILS ALREADY ENTERED FOR TERM 1
			} else if ((term == 2)) {
				select.addNewDetails("II", grade);
				break;
			} else if ((term == 3)) {
				select.addNewDetails("III", grade);
				break;
			} else {
				correction = 1;
				message.incorrectValue();
			}
		}
	}

	/**
	 * new students added by teacher from here.
	 * 
	 * @param term  form this parameter all of sections get which term is?..
	 * @param grade form this parameter all of sections get which grade is?..
	 */
	public void addNewDetails(String term, int grade) {
		PrintingMessages message = new PrintingMessages();
		Scanner input = new Scanner(System.in);
		message.studentDetailsAddNewDetails(1);
		// HOW MANY STUDENTS NEED TO ADD
		int numberOfStudent = input.nextInt();
		System.out.println();
		StudentDetails select = new StudentDetails();
		int[] indexNo = new int[numberOfStudent];
		String[] studentName = new String[numberOfStudent];
		int[] maths = new int[numberOfStudent];
		int[] physics = new int[numberOfStudent];
		int[] chemistry = new int[numberOfStudent];
		for (int i = 0; i < numberOfStudent; i++) {
			System.out.print("INDEX NO \t:");
			Scanner input1 = new Scanner(System.in);
			indexNo[i] = input1.nextInt();
			Scanner input2 = new Scanner(System.in);
			System.out.print("STUDENT NAME \t:");
			studentName[i] = input2.nextLine();
			maths[i] = 101;
			physics[i] = 101;
			chemistry[i] = 101;
			while ((maths[i] > 100) | (maths[i] < 0)) {
				Scanner input3 = new Scanner(System.in);
				System.out.print("MATHS \t\t:");
				maths[i] = input3.nextInt();
				if ((maths[i] > 100) | (maths[i] < 0)) {
					message.incorrectValue();
				}
			}

			while ((physics[i] > 100) | (physics[i] < 0)) {
				Scanner input4 = new Scanner(System.in);
				System.out.print("PHYSICS \t:");
				physics[i] = input4.nextInt();
				if ((physics[i] > 100) | (physics[i] < 0)) {
					message.incorrectValue();
				}
			}

			while ((chemistry[i] > 100) | (chemistry[i] < 0)) {
				Scanner input5 = new Scanner(System.in);
				System.out.print("CHEMISTRY \t:");
				chemistry[i] = input5.nextInt();
				if ((chemistry[i] > 100) | (chemistry[i] < 0)) {
					message.incorrectValue();
				}
			}

			System.out.println();
		}
		message.studentDetailsAddNewDetails(2);
		// NOW YOU CAN GET THE REPORT OF THE STUDENTS
		select.viewDetailsForAdminAndTeacher(indexNo, studentName, grade, maths, physics, chemistry, term);
	}

	/**
	 * @param type form this parameter we can select the type of who can access
	 *             student or teacher?
	 */
	public void grade12Term1StudentDetails(int type) {
		StudentDetails select = new StudentDetails();
		int[] indexNo = { 12001, 12002, 12003, 12004, 12005 };
		String[] studentName = { "J.Konsikan", "V.Mathy", "S.Silaxan", "S.Kannalan", "K.Sajanthan" };
		int grade = 12;
		int[] maths = { 48, 72, 100, 92, 48 };
		int[] physics = { 55, 92, 92, 42, 91 };
		int[] chemistry = { 75, 42, 91, 72, 82 };
		if (type == 1) {
			select.viewDetailsForAdminAndTeacher(indexNo, studentName, grade, maths, physics, chemistry, "I");
		}
		if (type == 2) {
			select.viewDetailsForStudent(indexNo, studentName, grade, maths, physics, chemistry, "I");
		}
	}

	/**
	 * @param type form this parameter we can select the type of who can access
	 *             student or teacher?
	 */
	public void grade13Term1StudentDetails(int type) {
		StudentDetails select = new StudentDetails();
		int[] indexNo = { 13001, 13002, 13003, 13004, 13005 };
		String[] studentName = { "K.Kathusha", "S.Suganya", "S.Vinoja", "T.Kamala", "K.Aksaya" };
		int grade = 13;
		int[] maths = { 41, 72, 89, 92, 98 };
		int[] physics = { 51, 61, 41, 82, 91 };
		int[] chemistry = { 45, 42, 41, 71, 12 };
		if (type == 1) {
			select.viewDetailsForAdminAndTeacher(indexNo, studentName, grade, maths, physics, chemistry, "I");
		}
		if (type == 2) {
			select.viewDetailsForStudent(indexNo, studentName, grade, maths, physics, chemistry, "I");
		}
	}

	/**
	 * In this section all student details going to be in a ranking order.
	 * 
	 * @param term form this parameter all of sections get which term is?..
	 * @param sINo it helps to find the specific student index no..
	 */
	public void rankedList(int[] indexNo, String[] studentName, int grade, int[] maths, int[] physics, int[] chemistry,
			String term, int typ, int sINo) {
		PrintingMessages message = new PrintingMessages();
		StudentDetails select = new StudentDetails();
		int tot[] = new int[indexNo.length];
		int temp;
		int sID;
		String sName;
		int sMaths;
		int sPhysics;
		int sChemistry;
		for (int i = 0; i < indexNo.length; i++) {
			int total = maths[i] + physics[i] + chemistry[i];
			tot[i] = total;
		}
		for (int j = 0; j < indexNo.length; j++) {
			for (int k = j + 1; k < indexNo.length; k++) {
				if (tot[j] < tot[k]) {
					temp = tot[j];
					tot[j] = tot[k];
					tot[k] = temp;

					sID = indexNo[j];
					indexNo[j] = indexNo[k];
					indexNo[k] = sID;

					sName = studentName[j];
					studentName[j] = studentName[k];
					studentName[k] = sName;

					sMaths = maths[j];
					maths[j] = maths[k];
					maths[k] = sMaths;

					sPhysics = physics[j];
					physics[j] = physics[k];
					physics[k] = sPhysics;

					sChemistry = chemistry[j];
					chemistry[j] = chemistry[k];
					chemistry[k] = sChemistry;
				}
			}
		}
		if (typ == 1) {
			// all student reports
			for (int n = 0; n < indexNo.length; n++) {
				if (n > 0) {
					if (tot[n] == tot[n - 1]) {
						message.studentDetailsRank(n);
						select.getDetails(indexNo[n], studentName[n], grade, maths[n], physics[n], chemistry[n]);
					}
				}
				if (n > 0) {
					if (tot[n] != tot[n - 1]) {
						message.studentDetailsRank(n + 1);
						select.getDetails(indexNo[n], studentName[n], grade, maths[n], physics[n], chemistry[n]);
					}
				}

			}
		}
		if (typ == 2) {
			// specific student report
			for (int i = 0; i < indexNo.length; i++) {
				int stINo = indexNo[i];
				if (sINo == stINo) {
					message.studentDetailsTerm(term);
					message.studentDetailsRank(i + 1);
					select.getDetails(indexNo[i], studentName[i], grade, maths[i], physics[i], chemistry[i]);
					break;
				} else {
					if (i == indexNo.length - 1) {
						message.incorrectValue();
					}
				}
			}
		}
	}

	/**
	 * teachers can access this section.
	 * 
	 * @param indexNo
	 * @param studentName
	 * @param grade
	 * @param maths
	 * @param physics
	 * @param chemistry
	 * @param term
	 */
	public void viewDetailsForAdminAndTeacher(int[] indexNo, String[] studentName, int grade, int[] maths,
			int[] physics, int[] chemistry, String term) {
		StudentDetails select = new StudentDetails();
		PrintingMessages message = new PrintingMessages();
		message.studentDetailsTerm(term);
		// TERM
		message.studentDetailsViewDetailsForAdminAndTeacher();
		// YOU CAN SEE THE STUDENT DETAILS HERE
		// 1-SPECIFIC STUDENT REPORT | 2-ALL STUDENT REPORTS
		// 4-HIGHER RANKER DETAILS IN EACH SUBJECTS
		int correction = 1;
		while (correction == 1) {
			message.type();
			Scanner input = new Scanner(System.in);
			int type = input.nextInt();
			System.out.println();
			if (type == 1) {
				select.specificStudentDetails(indexNo, studentName, grade, maths, physics, chemistry, term);
				break;
			} else if (type == 2) {
				select.allStudentDetails(indexNo, studentName, grade, maths, physics, chemistry, term);
				break;
			} else if (type == 3) {
				select.topperDetailsInEachSubjects(indexNo, studentName, grade, maths, physics, chemistry, term);
				break;
			} else {
				correction = 1;
				message.incorrectValue();
			}
		}
	}

	/**
	 * students can access this section.
	 * 
	 * @param indexNo
	 * @param studentName
	 * @param grade
	 * @param maths
	 * @param physics
	 * @param chemistry
	 * @param term
	 */

	public void viewDetailsForStudent(int[] indexNo, String[] studentName, int grade, int[] maths, int[] physics,
			int[] chemistry, String term) {
		StudentDetails select = new StudentDetails();
		PrintingMessages message = new PrintingMessages();
		message.studentDetailsTerm(term);
		// TERM
		message.studentDetailsViewDetailsForStudent();
		// YOU CAN SEE SPECIFIC DETAILS HERE
		// 1-REPORT CARD | 2-HIGHER RANKER DETAILS IN EACH SUBJECT
		int correction = 1;
		while (correction == 1) {
			message.type();
			Scanner input = new Scanner(System.in);
			int type = input.nextInt();
			System.out.println();
			if (type == 1) {
				select.specificStudentDetails(indexNo, studentName, grade, maths, physics, chemistry, term);
				break;
			} else if (type == 2) {
				select.topperDetailsInEachSubjects(indexNo, studentName, grade, maths, physics, chemistry, term);
				break;
			} else {
				correction = 1;
				message.incorrectValue();
			}
		}
	}

	public void allStudentDetails(int[] indexNo, String[] studentName, int grade, int[] maths, int[] physics,
			int[] chemistry, String term) {
		PrintingMessages message = new PrintingMessages();
		StudentDetails select = new StudentDetails();
		message.studentDetailsOthers(1);
		// YOU CAN SEE ALL STUDENT REPORTS HERE
		message.studentDetailsTerm(term);
		// TERM
		select.rankedList(indexNo, studentName, grade, maths, physics, chemistry, term, 1, 0);
	}

	public void specificStudentDetails(int[] indexNo, String[] studentName, int grade, int[] maths, int[] physics,
			int[] chemistry, String term) {
		StudentDetails select = new StudentDetails();
		PrintingMessages message = new PrintingMessages();
		message.studentDetailsOthers(2);
		int correction1 = 1;
		while (correction1 == 1) {
			System.out.println();
			Scanner input = new Scanner(System.in);
			System.out.print("INDEX NO : ");
			int sINo = input.nextInt();
			System.out.println();
			select.rankedList(indexNo, studentName, grade, maths, physics, chemistry, term, 2, sINo);
		}
	}

	public void topperDetailsInEachSubjects(int[] indexNo, String[] studentName, int grade, int[] maths, int[] physics,
			int[] chemistry, String term) {
		StudentDetails select = new StudentDetails();
		PrintingMessages message = new PrintingMessages();
		message.studentDetailsOthers(3);
		// YOU CAN SEE HIGHER RANKER DETAILS IN EACH SUBJECTS HERE
		// | 1-MATHS | 2-PHYSICS | 3-CHEMISTRY |
		int correction = 1;
		while (correction == 1) {
			Scanner input = new Scanner(System.in);
			message.type();
			int typeForSubject = input.nextInt();
			System.out.println();
			if (typeForSubject == 1) {
				String subjectName = "MATHS";
				select.topperInEachSubjects(indexNo, studentName, grade, maths, subjectName, term);
				break;
			} else if (typeForSubject == 2) {
				String subjectName = "PHYSICS";
				select.topperInEachSubjects(indexNo, studentName, grade, physics, subjectName, term);
				break;
			} else if (typeForSubject == 3) {
				String subjectName = "CHEMISTRY";
				select.topperInEachSubjects(indexNo, studentName, grade, chemistry, subjectName, term);
				break;
			} else {
				correction = 1;
				message.incorrectValue();
			}
		}
	}

	/**
	 * Toper In each subjects details made from here.
	 * 
	 * @param indexNo
	 * @param studentName
	 * @param grade
	 * @param mark
	 * @param subjectName
	 * @param term
	 */
	public void topperInEachSubjects(int[] indexNo, String[] studentName, int grade, int[] mark, String subjectName,
			String term) {
		PrintingMessages message = new PrintingMessages();
		StudentDetails grad = new StudentDetails();
		int maxMark = 0;
		int maxindexNo = 0;
		String maxStudentName = null;
		for (int i = 0; i < mark.length; i++) {
			if (mark[i] > maxMark) {
				maxMark = mark[i];
				maxindexNo = indexNo[i];
				maxStudentName = studentName[i];
			}
		}
		message.studentDetailsTerm(term);
		// TERM
		System.out.println("\t\t\t    **********************************");
		System.out.println("\t\t\t       TOP RANKED STUDENT IN " + subjectName);
		System.out.println("\t\t\t      ===============================");
		System.out.println("\t\t\t         INDEX NO \t: " + maxindexNo);
		System.out.println("\t\t\t         NAME \t        : " + maxStudentName);
		System.out.println("\t\t\t         GRADE \t        : " + grade);
		System.out.println("\t\t\t         MARKS \t        : " + maxMark + "\tGRADE");
		grad.grading(maxMark);
		System.out.println("\t\t\t    **********************************");
	}

	/**
	 * student reports made from here.
	 * 
	 * @param indexNo
	 * @param studentName
	 * @param grade
	 * @param maths
	 * @param physics
	 * @param chemistry
	 */
	public void getDetails(int indexNo, String studentName, int grade, int maths, int physics, int chemistry) {
		StudentDetails grad = new StudentDetails();
		int total = maths + physics + chemistry;
		int percent = total / 3;
		System.out.println();
		System.out.println("\t     *****************************************************************");
		System.out.println("\t\tINDEX NO \t:" + indexNo);
		System.out.println("\t\tSTUDENT NAME \t:" + studentName);
		System.out.println("\t\tGRADE \t\t:" + grade);
		System.out.println("\t     -----------------------------------------------------------------");
		System.out.println();
		System.out.println("\t\t\t     SUBJECT             MARKS \tGRADE");
		System.out.println();
		System.out.println("\t\t\t     MATHS                " + maths);
		grad.grading(maths);
		System.out.println("\t\t\t     PHYSICS              " + physics);
		grad.grading(physics);
		System.out.println("\t\t\t     CHEMISTRY            " + chemistry);
		grad.grading(chemistry);
		System.out.println("\t     -----------------------------------------------------------------");
		System.out.println("\t\t\t     TOTAL MARKS\t: " + total + "/300");
		System.out.println("\t\t\t     PERCENTAGE\t\t: " + percent);
		System.out.println("\t\t\t     OVERALL GRADE\t: ");
		grad.grading((int) percent);
		System.out.println("\t     =================================================================");
		System.out.println();
		System.out.println();
	}

	/**
	 * grading system.
	 * 
	 * @param marks from this we can make grading in each subjects.
	 */
	public void grading(int marks) {
		if ((marks <= 100) && (marks >= 75)) {
			System.out.println("\t\t\t\t\t\t\t|A|");
		}
		if ((marks <= 74) && (marks >= 65)) {
			System.out.println("\t\t\t\t\t\t\t|B|");
		}
		if ((marks <= 64) && (marks >= 55)) {
			System.out.println("\t\t\t\t\t\t\t|C|");
		}
		if ((marks <= 54) && (marks >= 35)) {
			System.out.println("\t\t\t\t\t\t\t|D|");
		}
		if (marks <= 34) {
			System.out.println("\t\t\t\t\t\t\t|F|");
		}
	}
}
