package reportCard;

/**
 * All printing messages are went from here.
 * 
 * @author J.Konsikan
 * @version 1.0
 *
 */
public class PrintingMessages {
	/**
	 * @param select this is for easy calling method..
	 */
	public void start(int select) {

		if (select == 1) {
			System.out.println();
			System.out.println("\t\t     | THIS REPORT CARD SYSTEM FOR A/L 12|13 STUDENTS |");
			System.out.println();
			System.out.println("\t\t\t\t     ##-MATHEMATICS-##");
			System.out.println();
		}
		if (select == 2) {
			System.out.println();
			System.out.println("\t\t\t\t\t =-GRADE-=");
			System.out.print("\t\t\t\t\t     ");
		}
		if (select == 3) {
			System.out.println();
			System.out.println("\t\t\t\t  1-TEACHER | 2-STUDENT |");
			System.out.println("\t\t\t\t  =======================");
		}
	}

	/**
	 * @param select this is for easy calling method..
	 */
	public void panels(int select) {
		if (select == 1) {
			System.out.println("\t\t\t\t       *************");
			System.out.println("\t\t\t\t       TEACHER PANEL");
			System.out.println("\t\t\t\t       *************");
		}
		if (select == 2) {
			System.out.println("\t\t\t\t       *************");
			System.out.println("\t\t\t\t       STUDENT PANEL");
			System.out.println("\t\t\t\t       *************");
		}
	}

	/**
	 * @param select this is for easy calling method..
	 */
	public void teacherPanel(int select) {

		if (select == 1) {
			System.out.println("\t\t\t ******************************************");
			System.out.println("\t\t\t     YOU NEED ADMIN PERMISSION TO CREAT");
			System.out.println("\t\t\t ******************************************");
		}
		if (select == 2) {
			System.out.println("\t\t\t     ********************************");
			System.out.println("\t\t\t           $$ ACCESS GRANTED $$");
			System.out.println("\t\t\t     ********************************");
		}
	}

	public void studentPanel() {
		System.out.println("\t\t\t******************************************");
		System.out.println("\t\t\t  YOU CAN SEE ONLY SPECIFIC DETAILS HERE");
		System.out.println("\t\t\t******************************************");
	}

	/**
	 * @param select this is for easy calling method..
	 */
	public void logInOrSignIn(int select) {
		if (select == 1) {
			System.out.println();
			System.out.println("\t\t\t     =================================");
			System.out.println("\t\t\t     | 1-LOGIN | 2-CREAT NEW ACCOUNT |");
			System.out.println("\t\t\t     =================================");
		}
		if (select == 2) {
			System.out.println("\t\t\t\t\t  =======");
			System.out.println("\t\t\t\t\t  [LOGIN]");
			System.out.println("\t\t\t\t\t  =======");
		}
		if (select == 3) {
			System.out.println("\t\t\t\t      ================");
			System.out.println("\t\t\t\t      [CREATE ACCOUNT]");
			System.out.println("\t\t\t\t      ================");
		}
		if (select == 4) {
			System.out.println();
			System.out.println("\t\t\t\t| $ YOU ARE LOGGED IN $ |");
			System.out.println();
		}
		if (select == 5) {
			System.out.println();
			System.out.println("\t\t\t  | !! INCORRECT USER NAME & PASSWORD !! |");
			System.out.println("\t\t\t              PLEASE TRY AGAIN");
		}
		if (select == 6) {
			System.out.println();
			System.out.println("\t\t\t$$ YOUR ACCOUNT WAS SUCCESSFULLY CREATED $$");
			System.out.println();
		}
	}

	public void type() {
		System.out.println();
		System.out.println("\t\t\t\t\t   TYPE!");
		System.out.print("\t\t\t\t\t     ");
	}

	public void incorrectValue() {
		System.out.println("\t\t\t\t   !! INCORRECT VALUE !!");
		System.out.println("\t\t\t\t        -TRY AGAIN-  ");
	}

	public void studentDetailsMain() {
		System.out.println();
		System.out.println("\t\t    ***************************************************");
		System.out.println("\t\t\t  YOU CAN SEE|ADD THE STUDENT DETAILS HERE");
		System.out.println("\t\t    ***************************************************");
		System.out.println();
		System.out.println("\t    1-ADD NEW STUDENT DETAILS | 2-VIEW PREVIOUS TERM STUDENT DETAILS");

	}

	/**
	 * @param select this is for easy calling method..
	 * @param term   this is for term selection..
	 */
	public void studentDetailsAddOrManageNewStudentDetails(int select, String term) {
		if (select == 1) {
			System.out.println();
			System.out.println("\t\t\t\t\t   TERM!");
			System.out.print("\t\t\t\t\t     ");
		}
		if (select == 2) {
			System.out.println("\t\t\t !! DETAILS ALREADY ENTERED FOR TERM " + term + " !!");
			System.out.println();
		}
	}

	/**
	 * @param select this is for easy calling method..
	 */
	public void studentDetailsAddNewDetails(int select) {
		if (select == 1) {
			System.out.println();
			System.out.println("\t\t\t     : HOW MANY STUDENTS NEED TO ADD : ");
			System.out.print("\t\t\t\t\t     ");
		}
		if (select == 2) {
			System.out.println("\t\t   ****************************************************");
			System.out.println("\t\t\t NOW YOU CAN GET THE REPORT OF THE STUDENTS");
			System.out.println("\t\t   ****************************************************");
		}

	}

	public void studentDetailsViewDetailsForAdminAndTeacher() {
		System.out.println("\t\t\t******************************************");
		System.out.println("\t\t\t   YOU CAN SEE THE STUDENT DETAILS HERE");
		System.out.println("\t\t\t******************************************");
		System.out.println("");
		System.out.println("\t\t     1-SPECIFIC STUDENT REPORT | 2-ALL STUDENT REPORTS");
		System.out.println("\t\t\t  3-HIGHER RANKER DETAILS IN EACH SUBJECTS ");
	}

	public void studentDetailsViewDetailsForStudent() {
		System.out.println("\t\t\t*******************************************");
		System.out.println("\t\t\t     YOU CAN SEE SPECIFIC DETAILS HERE");
		System.out.println("\t\t\t*******************************************");
		System.out.println();
		System.out.println("\t\t 1-REPORT CARD | 2-HIGHER RANKER DETAILS IN EACH SUBJECT");
	}

	/**
	 * @param select this is for easy calling method..
	 */
	public void studentDetailsOthers(int select) {
		if (select == 1) {
			System.out.println("\t\t\t******************************************");
			System.out.println("\t\t\t   YOU CAN SEE ALL STUDENT REPORTS HERE");
			System.out.println("\t\t\t******************************************");
			System.out.println();
		}
		if (select == 2) {
			System.out.println();
			System.out.println("\t\t\t*******************************************");
			System.out.println("\t\t       YOU CAN SEE THE SPECIFIC STUDENT REPORT HERE!");
			System.out.println("\t\t\t*******************************************");
		}
		if (select == 3) {
			System.out.println("\t\t\t*******************************************");
			System.out.println("\t\t  YOU CAN SEE HIGHER RANKER DETAILS IN EACH SUBJECTS HERE");
			System.out.println("\t\t\t*******************************************");
			System.out.println();
			System.out.println("\t\t\t   | 1-MATHS | 2-PHYSICS | 3-CHEMISTRY |");
		}
	}

	/**
	 * @param term this is for term selection..
	 */
	public void studentDetailsTerm(String term) {
		System.out.println("\t\t\t\t\t  =======");
		System.out.println("\t\t\t\t\t  TERM " + term);
		System.out.println("\t\t\t\t\t  =======");
	}

	/**
	 * @param rank this is for rank selection..
	 */
	public void studentDetailsRank(int rank) {
		System.out.println("\t\t\t\t\t  =======");
		System.out.println("\t\t\t\t\t  RANK-" + rank + "-");
		System.out.println("\t\t\t\t\t  =======");
	}
}
