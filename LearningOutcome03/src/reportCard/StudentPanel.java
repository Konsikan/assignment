package reportCard;

import java.util.Scanner;

/**
 * Student panel start from here.
 * 
 * @author J.Konsikan
 * @version 1.0
 *
 */
public class StudentPanel {
	public void start() {
		PrintingMessages message = new PrintingMessages();
		LogInSignIn loginOrSignIn = new LogInSignIn();
		message.panels(2);
		// STUDENT PANEL
		message.logInOrSignIn(1);
		// 1-LOGIN | 2-CREAT NEW ACCOUNT
		Scanner input = new Scanner(System.in);
		int correction = 1;
		while (correction == 1) {
			message.type();
			int type = input.nextInt();
			System.out.println();
			if (type == 1) {
				String username = "ST01";
				String password = "STBCAS01";
				loginOrSignIn.logIn(username, password);
				break;
			} else if (type == 2) {
				loginOrSignIn.signIn();
				break;
			} else {
				correction = 1;
				message.incorrectValue();
			}
		}
	}
}
