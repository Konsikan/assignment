package reportCard;

import java.util.Scanner;

/**
 * 
 * Main system start from here.
 * 
 * @author J.Konsikan
 * @version 1.0
 * @since 22.06.2020
 *
 */
public class Main {

	public static void main(String args[]) {
		PrintingMessages message = new PrintingMessages();
		StudentDetails calling = new StudentDetails();
		message.start(1);
		// THIS REPORT CARD SYSTEM FOR A/L 12|13 STUDENTS
		int correction = 1;
		while (correction == 1) {
			Scanner input = new Scanner(System.in);
			message.start(2);
			// GRADE
			int grade = input.nextInt();
			System.out.println();
			if ((grade == 12) | (grade == 13)) {
				message.start(3);
				// 1-TEACHER | 2-STUDENT |
				int correction1 = 1;
				while (correction1 == 1) {
					Scanner input1 = new Scanner(System.in);
					message.type();
					int type = input1.nextInt();
					System.out.println();
					if (type == 1) {
						TeacherPanel teacher = new TeacherPanel();
						teacher.start();
						calling.teacherPanel(grade);
						break;
					} else if (type == 2) {
						StudentPanel student = new StudentPanel();
						student.start();
						if (grade == 12) {
							calling.grade12Term1StudentDetails(2);
						}
						if (grade == 13) {
							calling.grade13Term1StudentDetails(2);
						}
						break;
					} else {
						correction1 = 1;
						message.incorrectValue();
					}
				}
				break;
			} else {
				correction = 1;
				message.incorrectValue();
			}
		}
	}
}
