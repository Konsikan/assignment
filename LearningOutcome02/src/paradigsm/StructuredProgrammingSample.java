package paradigsm;

/**
 * Structured Programming Sample
 * 
 * @author J.Konsikan
 *
 */
public class StructuredProgrammingSample {
	public static void main(String[] args) {
		/**
		 * sequence
		 */
		int x = 5;
		int y = 11;
		int z = x + y;
		System.out.println(z);
		/**
		 * repetition
		 */
		int n = 5;
		while (n < 100) {
			System.out.println(n);
			n = n * n;
		}
		/**
		 * decision
		 */
		int p = 5;
		if (p % 2 == 0) {
			System.out.println("THE NUMBER IS EVEN");
		}

	}
}
