package paradigsm;

/**
 * Procedural Programming Sample
 * 
 * @author J.Konsikan
 *
 */
public class ProceduralProgrammingSample {
	public static void main(String[] args) {
		ProceduralProgrammingSample demo = new ProceduralProgrammingSample();
		demo.main();
	}

	int main() {
		int sum = 0;
		int i = 0;
		for (i = 1; i < 11; i++) {
			sum += i;
		}
		System.out.println("THE SUM IS " + sum);
		// prints-> THE SUM IS 55
		return 0;
	}
}
