package paradigsm;

/**
 * Object oriented Programming Sample
 * 
 * @author J.Konsikan
 *
 */
public class ObjectOrientedProgrammingSample {
	public static void main(String[] args) {
		Sum obj = new Sum();
		obj.number = 10;
		int answer = obj.addValues();
		System.out.println("THE SUM IS = " + answer); // prints-> The sum is 55
	}
}

class Sum {
	int sum = 0;
	int number = 0;

	int addValues() {
		for (int i = 1; i <= number; i++) {
			sum += i;
		}
		return sum;
	}
}
