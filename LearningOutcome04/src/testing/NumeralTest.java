package testing;

import org.junit.Assert;
import org.junit.Test;

public class NumeralTest {

	Numeral numeral = new Numeral();

	@Test
	public void DecimalToBinary_Test() {
		Assert.assertEquals("1 0 1 0 ", numeral.DecimalToBinary(10));
	}

	@Test
	public void BinaryToDecimal_Test() {
		Assert.assertEquals(12, numeral.BinaryToDecimal("1100"));
	}

}
