package testing;

public class Numeral {

	public String DecimalToBinary(int decimal) {
		String binary = "";
		while (decimal > 0) {
			int i = decimal % 2;
			binary = i + " " + binary;
			decimal = decimal / 2;
		}
		return binary;
	}

	public int BinaryToDecimal(String binary) {
		int n = binary.length();
		int decimal = 0;
		for (int i = n - 1; i >= 0; i--) {
			if (binary.charAt(i) == '1') {
				decimal = decimal + (int) Math.pow(2, (n - (i + 1)));
			}
		}
		return decimal;
	}

}
