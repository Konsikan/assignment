
package algorithmDemo;

import java.util.Scanner;

/**
 * Java program to convert Fahrenheit to Celsius.
 * 
 * @author J.Konsikan
 * 
 */

public class FahrenheitToCelsius {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);

		/** Input temperature in Fahrenheit from user */
		System.out.print("ENTER TEMPERATURE IN FAHRENHEIT: ");
		float F = in.nextFloat();

		/** Convert Fahrenheit to Celsius */
		float C = (F - 32) * (9f / 5);

		/** Print temperature in Celsius */
		System.out.println(F + " DEGREE FAHRENHEIT IS EQUAL TO " + C + " DEGREE CELSIUS.");

	}
}
